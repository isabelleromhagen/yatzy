package main;


public class Dice {


    public int[] rollDice(int rolls, int[] faceValues){


        for (int i = 0; i < rolls; i++) {
            int outcome = (int)(Math.random() * 6 + 1);
            faceValues[i] = outcome;
            System.out.println("Face value " + (i + 1) + ": " + outcome);
        }

        return faceValues;
    }

}
