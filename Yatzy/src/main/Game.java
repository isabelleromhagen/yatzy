package main;

import java.util.Scanner;

class Game {
    private Scanner sc = new Scanner(System.in);
    private Player player1 = new Player();
    private Player player2 = new Player();
    private Dice dice = new Dice();
    private int[] faceValues = new int[5];
    private String[] categories = {"Ones", "Twos", "Threes", "Fours", "Fives", "Sixes", "Three of a kind", "Four of a kind", "Full house", "Small straight", "Large straight", "Yahtzee", "Chance"};
    private int points, rolls, rounds;

    Game(){
       // startGame();
    }

    private void startGame(){
        System.out.println("Welcome to yahtzee, let's play!");
        player1.points = 0;
        player1.name = "Player 1";
        player2.points = 0;
        player2.name = "Player 2";
        rounds = 1;

        while(rounds < 13){
            Player currentPlayer = player1;
            startNewTurn(currentPlayer);




        }
    }

    private void startNewTurn(Player currentPlayer){

        System.out.println(currentPlayer.name + " rolls dices");

        faceValues = dice.rollDice(faceValues.length, faceValues);
        System.out.println("Current face values: ");
        printFaceValues(faceValues);

        for(int i = 0; i < 2; i++){

            System.out.println("Enter or 0 to end turn.");

            int dieToRoll = sc.nextInt();
            if (dieToRoll > 0){
                dice.rollDice(dieToRoll, faceValues);
            }
            else break;
        }
        System.out.println(currentPlayer.name + " is done rolling dices for this round");

        System.out.println("Which category do you want to fill?");
        printCategories();
        int categoryIndex = sc.nextInt();

        System.out.println("To be removed: " + categories[categoryIndex]);
        int score = calculatePoints(categoryIndex, faceValues);
        System.out.println("Points added to " + currentPlayer.name + ": " + score);
        System.out.println(currentPlayer.name + " now has a total of " + player1.addPoints(player1.points, score) + " points.");
    }

    public int calculatePoints(int categoryIndex, int[] faceValues){
        int score = 0;

        switch (categoryIndex){
            case 0:
                for (int faceValue : faceValues) {
                    if (faceValue == 1) {
                        score++;
                    }

                }
                break;
            case 1:
                for(int faceValue : faceValues) {
                    if(faceValue == 2){
                        score += 2;
                    }
                }
                break;
            case 2:
                for(int faceValue : faceValues){
                    if(faceValue == 3){
                        score += 3;
                    }
                }
                break;
            case 3:
                for(int faceValue : faceValues){
                    if(faceValue == 4){
                        score += 4;
                    }
                }

                break;
            case 4:
                for(int faceValue : faceValues){
                    if(faceValue == 5){
                        score += 5;
                    }
                }
                break;
            case 5:
                for(int faceValue : faceValues){
                    if(faceValue == 6){
                        score += 6;
                    }
                }
                break;
            case 6:
                int count = 0;
                for (int i = 0; i < faceValues.length; i++) {
                    for (int j = i + 1; j < faceValues.length; j++) {
                        if (faceValues[i] == faceValues[j]){
                            count++;
                        }
                    }
                }
                if (count > 2){
                    for(int faceValue : faceValues){
                        score += faceValue;
                    }
                }
                else System.out.println("Not enough of same, no points will be given.");
                break;
            case 7:
                count = 0;
                for (int i = 0; i < faceValues.length; i++){
                    for(int j = i + 1; j < faceValues.length; j++){
                        if(faceValues[i] == faceValues[j]){
                            count++;
                        }
                    }
                }
                if (count > 3){
                    for(int faceValue : faceValues){
                        score += faceValue;
                    }
                }
                else System.out.println("Not enough of same, no points will be given.");
                break;
            case 8:
                count = 0;
                int dieFound = 0;
                for (int i = 0; i < faceValues.length; i++) {
                    for (int j = i + 1; j < faceValues.length; j++) {
                        if (faceValues[i] == faceValues[j]){
                            count++;
                            dieFound = faceValues[i];
                        }
                    }
                }
                if (count < 3){
                    System.out.println("Not enough of same, no points will be given.");
                    break;
                }
                faceValues = removeFromArray(faceValues, dieFound);
                count = 0;

                for (int i = 0; i < faceValues.length; i++) {
                    for (int j = i + 1; j < faceValues.length; j++) {
                        if (faceValues[i] == faceValues[j]){
                            count++;
                        }
                    }
                }
                if (count < 2){
                    System.out.println("Not enough of same, no points will be given.");
                    break;
                }

                score = 25;
                break;
            case 9:
                    for (int i = 0; i < faceValues.length-2; i++){
                        if (faceValues[i] == 1){
                            if (faceValues[i+1] == 2 && faceValues[i+2] == 3 && faceValues[i+3] == 4){
                                score = 30;
                                break;
                            }
                            else
                                System.out.println("No legit straight found.");
                            break;
                        }
                        else if (faceValues[i] == 2){
                            if(faceValues[i+1] == 3 && faceValues[i+2] == 4 && faceValues[i+3] == 5){
                                score = 30;
                                System.out.println("Found straight starting with 2");
                                break;
                            }
                            else
                                System.out.println("No legit straight found.");
                            break;
                        }
                        else if (faceValues[i] == 3){
                            if (faceValues[i+1] == 4 && faceValues[i+2] == 5 && faceValues[i+3] == 6){
                                score = 30;
                                System.out.println("Found straight starting with 3");
                                break;
                            }
                        }

                    }

                break;
            case 10:
                for (int i = 0; i < faceValues.length; i++){
                    if (faceValues[i] == 1){
                        if (faceValues[i+1] == 2 && faceValues[i+2] == 3 && faceValues[i+3] == 4 && faceValues[i+4] == 5){
                            score = 40;
                            break;
                        }
                    }
                    else if (faceValues[i] == 2){
                        if(faceValues[i+1] == 3 && faceValues[i+2] == 4 && faceValues[i+3] == 5 && faceValues[i+4] == 6){
                            score = 40;
                            break;
                        }
                    }

                }

                break;
            case 11:
                for (int i = 0; i < faceValues.length; i++) {
                    for (int j = i + 1; j < faceValues.length; j++) {
                        if (faceValues[i] != faceValues[j]){
                            System.out.println("Not enough of same, no points will be given.");
                            break;
                        }
                    }
                }
                score = 50;
                break;
            case 12:
                for(int faceValue : faceValues){
                    score += faceValue;
                }
                break;
            default:
                System.out.println("Something went wrong.");

        }
        return score;
    }

    public int[] removeFromArray(int[] array, int index){
        int[] newArray = new int[array.length-1];

        for (int i = 0, j = 0; i < newArray.length; i++) {
            if (i == index){
                continue;
            }
            newArray[j++] = array[i];
        }
        return newArray;
    }

    private void printCategories(){
        for (String category : categories) {
            System.out.println(category);
        }
    }

    private void printFaceValues(int[] faceValues){
        for (int faceValue : faceValues) {
            System.out.println(faceValue);
        }
    }
}
