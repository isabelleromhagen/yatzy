package main;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class GameTest {

    Game game = new Game();

    @Test
    public void testCalculatePoints(){
        int[] testOnes = {4,3,4,5,6};
        Assert.assertEquals("Something went wrong.", 40, game.calculatePoints(10, testOnes));
    }

}